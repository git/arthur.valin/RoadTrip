<p align="center">
  <img src="Documentation/banner.png"  />
</p>

![Kotlin](https://img.shields.io/badge/Kotlin-7F52FF.svg?style=for-the-badge&logo=Kotlin&logoColor=white)
![Android Studio](https://img.shields.io/badge/Android%20Studio-3DDC84.svg?style=for-the-badge&logo=Android-Studio&logoColor=white)
![Android](https://img.shields.io/badge/Android-3DDC84.svg?style=for-the-badge&logo=Android&logoColor=white)</br>


**RoadTrip** est une application mobile Android qui permet aux utilisateurs de créer des itinéraires de voyage.

![](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/water.png)


## 🪶 Fonctionnalités

Lors de votre arrivée sur l'application, vous retrouverez une carte avec votre emplacement actuel. </br>Naviguez sur la carte pour découvrir vos environs. 

Appuyez 2 fois sur la carte pour créer un point d'intérêt, ajoutez en plusieurs afin de construire un itinéraire.

Validez ensuite votre voyage en cliquant sur le bouton ✅ en bas à gauche de l'écran et donnez lui un nom.

Retrouvez vos voyages dans l'onglet "Roadtrips" en bas de l'écran.

Accédez aux informations de votre voyage en cliquant dessus dans la liste déroulante. </br>Vous retrouverez la liste des points d'intérêts que vous avez ajouté, ainsi que les adresses de chacun d'entre eux.

Vous pouvez supprimer un voyage à tout moment avec le bouton en bas de votre écran</br>Ou vous pouvez l'éditer en rajoutant des points sur la carte et en validant.



![](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/water.png)

## 💽 Lancer l'application

Une fois le dépot cloné, vous pourrez lancer l'application sur votre téléphone Android grâce aux outils fournis par Android Studio. Si vous n'avez pas de téléphone Android, vous pouvez utiliser un émulateur.

![](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/water.png)

## 🤖 Made by 

Arthur VALIN : **Arthur.VALIN@etu.uca.fr**

<a href = "https://codefirst.iut.uca.fr/git/arthur.valin">
<img src ="https://codefirst.iut.uca.fr/git/avatars/041c57af1e1d1e855876d8abb5f1c143?size=870" height="50px">
</a>
<br/>
<br/>

Baptiste BONNEAU : **Baptiste.BONNEAU@etu.uca.fr**

<a href = "https://codefirst.iut.uca.fr/git/baptiste.bonneau">
<img src ="https://codefirst.iut.uca.fr/git/avatars/e47d41c01439fc439a23cf6843310b05?size=870" height="50px">
</a>








